<?php
    abstract class db_model {
      private static $db_host = 'localhost';
      private static $db_user = 'root';
      private static $db_pass = '';
      protected $db_name = 'usuarios';
      protected $query;
      protected $filas = array();
      private $conn;

      abstract protected function obtener($iduser,$pass);

      private function open_connection(){
        $this->conn = mysqli_connect(self::$db_host, self::$db_user,
                      self::$db_pass, $this->db_name);
      }

      private function close_connection(){
        $this->conn->close();
      }

    protected function get_results_from_query(){
      $this->open_connection();
      $result = $this->conn->query($this->query);
      while($this->filas[] = $result->fetch_assoc());
      $result->close();
      $this->close_connection();
      echo $this->filas->nombre;
      array_pop($this->filas);
    }
  }
?>
