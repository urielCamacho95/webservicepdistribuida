<?php
  require_once('db_u_abstract1.php');
  class usuario_2 extends db_model2 {
    public $saldo;
    public $l = array();//declarando array para codificar con formato JSON
    //Constructor
    function usuario_2(){
      $this->db_name = 'Banco2';
    }
    //Funcion para validar que el usuario existe
    function obtener($iduser){
        $this->query="
        SELECT *
        FROM usuarios
        WHERE id='$iduser'";
        $this->get_results_from_query();
      if(count($this->filas)==0){
        $l = array('No existe el usuario');
      }else{
          $l= array('Exito');
      }
      return $l;
    }

    /*Funcion para retirar, hace uso de la funcion consulta
    */
    function retiro($cantidad, $idus){
      $arrayAux = $this->consulta($idus);
      $cantAux = (float)array_pop($arrayAux);
      if($cantAux<$cantidad){
       $l= array("saldo insuficiente");
      }else{
        $cantAux -= $cantidad;
        $this->query = "
        UPDATE usuarios
        SET saldo = '$cantAux'
        WHERE id = '$idus'";
        $this->execute_single_query();
        $this->filas = array();
        $fecha = date("Y-m-d");
        $this->query = "
        INSERT INTO transacciones
        (fecha, movimiento, cantidad, fromUser,toUser)
        VALUES ('$fecha','Realizo un retiro', '$cantidad', '$idus', '$idus')";
        $this->execute_single_query();
        $this->query = "
        SELECT nombre,numeroCelular
        FROM usuarios
        WHERE id = '$idus'";
        $this->get_results_from_query();
        $arrAux = array($this->filas[0][numeroCelular]);
        $l=array("retiro realizado");
        array_push($l,array_pop($arrAux));
      }
      return $l;
    }
    function consulta($idus){
      $this->query = "
      SELECT saldo
      FROM usuarios
      WHERE id = '$idus'";
      $this->get_results_from_query();
      $l = array($this->filas[0][saldo]);
      return $l;
    }
    //Son los movimientos bancarios que ha hecho el usuario
    function movimientos($idus){
      $arrayAux = array();
      $l = $this->consulta($idus);
      $this->query = "
      SELECT *
      FROM transacciones
      WHERE fromUser = '$idus' OR
      toUser = '$idus'";
      $this->get_results_from_query();
      for ($i=1; $i<sizeof($this->filas);$i++) {
        $arrayAux = array();
        array_unshift($arrayAux, $this->filas[$i][movimiento]);
        array_unshift($arrayAux, $this->filas[$i][cantidad]);
        array_unshift($arrayAux, $this->filas[$i][fromUser]);
        array_unshift($arrayAux, $this->filas[$i][toUser]);
        array_unshift($arrayAux, $this->filas[$i][fecha]);
        array_unshift($l, $arrayAux);
      }
      return $l;
    }
    function deposito($cantidad,$idus){
      $arrayAux = $this->consulta($idus);
      $cantAux = (float)array_pop($arrayAux);
      $cantAux += $cantidad;
      $this->query = "
      UPDATE usuarios
      SET saldo = '$cantAux'
      WHERE id = '$idus'";
      $this->execute_single_query();
      $fecha = date("Y-m-d");
      $this->query = "
      INSERT INTO transacciones
      (fecha, movimiento,cantidad,fromUser,toUser)
      VALUES ('$fecha','Realizo un deposito', '$cantidad', '$idus', '$idus')";
      $this->execute_single_query();
      $this->query = "
      SELECT nombre,numeroCelular
      FROM usuarios
      WHERE id = '$idus'";
      $this->filas="";
      $this->get_results_from_query();
      $arrAux = array($this->filas[0][numeroCelular]);
      $l=array("deposito realizado");
      array_push($l,array_pop($arrAux));
      //var_dump($this->filas);
      return $l;

    }
    function deposito2($cantidad,$idus){
      $arrayAux = $this->consulta($idus);
      $cantAux = (float)array_pop($arrayAux);
      $cantAux += $cantidad;
      $this->query = "
      UPDATE usuarios
      SET saldo = '$cantAux'
      WHERE id = '$idus'";
      $this->execute_single_query();
      return $l=array('Exito');
    }
    function obtenCel($idus){
      $this->query = "
      SELECT nombre,numeroCelular
      FROM usuarios
      WHERE id = '$idus'";
      $this->filas="";
      $this->get_results_from_query();
      $l = array($this->filas[0][numeroCelular]);
      //echo "L ".$l;
      return $l;
    }
    function reciboTransaccion($from,$to,$cantidad){
      $fecha = date("Y-m-d");
      $this->query = "
      INSERT INTO transacciones
      (fecha, movimiento,cantidad,fromUser,toUser)
      VALUES ('$fecha',' Transferencia a ', $cantidad,'$from', '$to')";
      $this->execute_single_query();
      $this->query = "
      SELECT nombre,numeroCelular
      FROM usuarios
      WHERE id = '$to'";
      $this->filas="";
      $this->get_results_from_query();
      $arrAux = array($this->filas[0][numeroCelular]);
      $l=array('Exito');
      array_push($l,array_pop($arrAux));
      return $l;
    }
    function transferencia($ownid, $id_tarjeta, $cantidad){
      $arrayAux = $this->obtener($ownid);
      $opc = array_pop($arrayAux);
      $l=array('Error1');
      if($opc == 'Exito'){//verificar que ownid existe en la BD actual
        $arrayAux = $this->consulta($ownid);
        $cantAux = (float)array_pop($arrayAux);
        if($cantAux<$cantidad){//verificar que puede realizarse un retiro de ownid
         $l= array("saldo insuficiente");
        }else{
          $l=array("puede retirar");
        }
        $opc = array_pop($l);
        if($opc =='puede retirar'){//retirar de ownid se verifica que exista usuario2 (id_tarjeta) en la BD actual
          $this->filas = "";
          $arrayAux = $this->obtener($id_tarjeta);
          $opc = array_pop($arrayAux);
          //echo "usuario ".$opc;
          //var_dump($this->filas);
          if($opc=='Exito'){//Existe usuario 2 en el mismo banco
            $arrayAux = $this->consulta($ownid);//Se obtiene el saldo de ownid
            $cantAux = (float)array_pop($arrayAux);//Se castea a flotante y se almacena en una variable auxiliar
              $cantAux -= $cantidad;//Se resta la cantidad que tiene menos la cantidad a transferir
              $this->query = "
              UPDATE usuarios
              SET saldo = '$cantAux'
              WHERE id = '$ownid'";
              $this->execute_single_query();//Actualizar saldo de ownid
              $arrayAux = $this->deposito2($id_tarjeta);//Se realiza un deposito del tipo2(no se guarda como deposito en registro transacciones)
              $opc = array_pop($arrayAux);
              //var_dump($opc);
              if($opc=='Exito'){//Si se ejecuto correctamente
                $l = $this->reciboTransaccion($ownid,$id_tarjeta,$cantidad);//Se guarda la transaccion como una nueva transferencia
              }
          }else{//Se prueba en el otro banco
          //echo "hola666666";
          $url="http://localhost/banco/cuentas1/loginus1.php";//Se cambia el url al otro webservice
          $url2 = "?opcion=5&cuenta=".$id_tarjeta;//Se verifica que el usuario exista, opcion 5
          $json = file_get_contents($url.$url2);//Se obtiene el json codificado
          //echo "URL: ".$url.$url2;
          $l = json_decode($json);//se decodifica
          $verificar = array_pop($l);
          //echo "verifica ".$verificar;
          if($verificar=='Exito'){//Si existe usuario_2 en la otra BD
              $arrayAux = $this->consulta($ownid);//Se consulta nuevamente el saldo de ownid
              $cantAux = (float)array_pop($arrayAux);//Se castea a flotante
              $cantAux -= $cantidad;
              $this->query = "
              UPDATE usuarios
              SET saldo = '$cantAux'
              WHERE id = '$ownid'";
              $this->execute_single_query();//Se hace el retiro
              $url2="?opcion=7&cant=".$cantidad."&cuenta=".$id_tarjeta;//La opcion 7 hace un deposito sin guardarlo como tal en transacciones
              $json = file_get_contents($url.$url2);
              $l = json_decode($json);
              $verificar = array_pop($l);
              if($verificar=='Exito'){
                $url2="?opcion=6&cant=".$cantidad."&cuenta2=".$ownid."&cuenta=".$id_tarjeta;//Se hace una transaccion tipo transferencia
                $json = file_get_contents($url.$url2);
                //echo "!!!URL: ".$url.$url2;
                $l = json_decode($json);
                $verificar = array_shift($l);
                $aux = array_shift($l);
                //echo "verificar";
                //var_dump($verificar);
                if($verificar=='Exito'){
                  //echo "ENTRA";
                  //$l=array('Exito');
                  $l = $this->reciboTransaccion($ownid,$id_tarjeta,$cantidad);//Se guarda la transaccion como una nueva transferencia
                  $l=array('Exito');
                  array_push($l,$aux);
                  $aux = $this->obtenCel($ownid);
                  array_push($l,array_pop($aux));
                  //array_push($l,array_pop($arrAux));
                }else{
                  $l=array('Error2');
                }
              }else{
                $l=array('Error3');
              }
            }
          }
        }
      }
      return $l;
    }

}
?>
