<?php
    abstract class db_model1 {
    private static $db_host = 'localhost';
    private static $db_user = 'root';
    private static $db_pass = '';
    protected $db_name = 'Banco1';
    protected $query;
    protected $filas = array();
    private $conn;
    protected $cuenta;
    abstract protected function deposito($cantidad, $idus);
    abstract protected function deposito2($cantidad, $idus);
    abstract protected function consulta($id);
    abstract protected function retiro($cantidad,$idus);
    abstract protected function obtener($iduser);
    abstract protected function obtenCel($idus);
    abstract protected function movimientos($idus);
    abstract protected function transferencia($ownid, $id_tarjeta, $cantidad);
    abstract protected function reciboTransaccion($from,$to,$cantidad);
    private function open_connection(){
      $this->conn = mysqli_connect(self::$db_host, self::$db_user,
                      self::$db_pass, $this->db_name);
    }
    protected function execute_single_query() {
      $this->open_connection();
      $this->conn->query($this->query);
      $this->close_connection();
}

    protected function get_results_from_query(){
      $this->open_connection();
      $result = $this->conn->query($this->query);
      while($this->filas[] = $result->fetch_assoc());
      $result->close();
      $this->close_connection();
      array_pop($this->filas);
    }
    private function close_connection(){
      $this->conn->close();
    }
  }
?>
